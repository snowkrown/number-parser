﻿using System;

namespace NumberParser
{
	public class Converter
	{
		private NumbersContainer container;

		public Converter ()
		{
			container = new NumbersContainer ();
		}

		public string GetSum (string valueA, string valueB)
		{
			int parsedValueA = GetIntFromString (valueA);
			int parsedValueB = GetIntFromString (valueB);
			int total = parsedValueA + parsedValueB;

			return GetStringFromInt (total);
		}

		private int GetIntFromString (string value)
		{
			string[] splits = value.Split (' ');

			if (splits.Length > 0)
				return GetSumOfSplits (splits);

			return container.GetIntFromDictionary (value);
		}

		private int GetSumOfSplits (string[] splits)
		{
			int final = 0;

			for (int i = 0; i < splits.Length; i++)
			{
				if (final < container.GetIntFromDictionary (splits [i]))
					final = container.GetIntFromDictionary (splits [i]);
				else
					final += container.GetIntFromDictionary (splits [i]);
			}
			
			return final;
		}

		private string GetStringFromInt (int number)
		{
			int units = (number % 10);
			int tens = (number / 10);
			int thous = (tens / 10);

			if (thous > 0)
				return GetSumOfThous (units, tens, thous);

			if (tens > 1 && units != 0)
				return GetSumOfTens (number, units);

			return container.GetStringFromDictionary (number);
		}

		private string GetSumOfThous (int units, int tens, int thous)
		{
			string final = container.GetStringFromDictionary (thous) + " " + container.GetThousString (thous);
			int tensRestOnThous = ((tens * 10) - (thous * 100));

			if (units != 0)
				final += GetUnitsCountForThous (units, tens, tensRestOnThous);

			if (units == 0 && tens > 10)
				final += " " + container.GetStringFromDictionary (tensRestOnThous);

			return final;
		}

		private string GetUnitsCountForThous (int units, int tens, int tensRestOnThous)
		{
			if (tens < 11)
				return " " + container.GetStringFromDictionary (units);
			else if (tens == 11)
				return " " + container.GetStringFromDictionary (tensRestOnThous + units);
			else
				return " " + GetSumOfTens (tensRestOnThous + units, units);
		}

		private string GetSumOfTens (int number, int units)
		{
			int restant = (number - units);
			return container.GetStringFromDictionary (restant) + " " + container.GetStringFromDictionary (units);
		}
	}
}