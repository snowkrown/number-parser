﻿using System;
using System.Collections.Generic;

namespace NumberParser
{
	public class NumbersContainer
	{
		private Dictionary <string, int> units = new Dictionary <string, int> ();
		private Dictionary <string, int> teens = new Dictionary <string, int> ();
		private Dictionary <string, int> tens = new Dictionary <string, int> ();
		private Dictionary <string, int> thous = new Dictionary <string, int> ();

		public NumbersContainer ()
		{
			GenerateUnits ();
			GenerateTeens ();
			GenerateTens ();
			GenerateThous ();
		}

		private void GenerateUnits ()
		{
			units = new Dictionary<string, int> () 
			{
				{"Zero", 0}, {"One", 1}, {"Two", 2}, {"Three", 3}, {"Four", 4}, 
				{"Five", 5}, {"Six", 6}, {"Seven", 7}, {"Eight", 8}, {"Nine", 9},
			};
		}

		private void GenerateTeens ()
		{
			teens = new Dictionary<string, int> () 
			{
				{"Ten", 10}, {"Eleven", 11}, {"Twelve", 12}, {"Thirteen", 13}, {"Fourteen", 14}, 
				{"Fifteen", 15}, {"Sixteen", 16}, {"Seventeen", 17}, {"Eighteen", 18}, {"Nineteen", 19},
			};
		}

		private void GenerateTens ()
		{
			tens = new Dictionary<string, int> () 
			{
				{"Twenty", 20}, {"Thirty", 30}, {"Forty", 40}, {"Fifty", 50},
				{"Sixty", 60}, {"Seventy", 70}, {"Eighty", 80}, {"Ninety", 90}
			};
		}

		private void GenerateThous ()
		{
			thous = new Dictionary<string, int> () 
			{
				{"Hundred", 100}, {"Thousand", 1000}, {"Million", 1000000}, {"Billion", 1000000000},
			};
		}

		public int GetIntFromDictionary (string key)
		{
			if (units.ContainsKey (key)) return units [key];
			if (teens.ContainsKey (key)) return teens [key];
			if (tens.ContainsKey (key)) return tens [key];
			if (thous.ContainsKey (key)) return thous [key];

			return -214748364;
		}

		public string GetThousString (int thousValue)
		{
			if (thousValue < 10)
				return ExtractKeyFromDictionary (thous, 100);
			
			return "Invalid Operation";
		}

		public string GetStringFromDictionary (int value)
		{
			Dictionary <string, int> dictionary = GetDictionaryBasedOnValue (value);

			if (dictionary.ContainsValue (value))
				return ExtractKeyFromDictionary (dictionary, value);

			return "Invalid Operation for value " + value;
		}

		private string ExtractKeyFromDictionary (Dictionary<string, int> dictionary, int value)
		{
			foreach (string key in dictionary.Keys) 
			{
				int outValue = 0;

				if (dictionary.TryGetValue (key, out outValue) && outValue == value)
					return key;
			}

			return "Invalid Operarion";
		}

		private Dictionary<string, int> GetDictionaryBasedOnValue (int value)
		{
			if (units.ContainsValue (value)) return units;
			if (teens.ContainsValue (value)) return teens;
			if (tens.ContainsValue (value)) return tens;
			if (thous.ContainsValue (value)) return thous;

			return new Dictionary <string, int> ();
		}
	}
}

