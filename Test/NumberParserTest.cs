﻿using System;
using NumberParser;
using NUnit.Framework;

namespace NumberParserUnitTest
{
	[TestFixture]
	public class NumberParserTest
	{
		private Converter converter;

		[SetUp]
		public void SetUp ()
		{
			converter = new Converter ();
		}

		[Test]
		public void IfSendAInvalidNumberReturnsZero ()
		{
			MakeSum ("Invalid Operation for value -429496723", "Five", "Fou rty");
		}

		[Test]
		public void IfSendOnePlusOneReturnsTwo ()
		{
			MakeSum ("Two", "One", "One");
		}

		[Test]
		public void IfSendOnePlusTwoReturnsThree ()
		{
			MakeSum ("Three", "One", "Two");
		}

		[Test]
		public void IfSendTwoPlusTwoReturnsFour ()
		{
			MakeSum ("Four", "Two", "Two");
		}

		[Test]
		public void IfSendTwoPlusThreeReturnsFive ()
		{
			MakeSum ("Five", "Two", "Three");
		}

		[Test]
		public void IfSendFivePlusFiveReturnsTen ()
		{
			MakeSum ("Ten", "Five", "Five");
		}

		[Test]
		public void IfSendFivePlusFourReturnsNine ()
		{
			MakeSum ("Nine", "Five", "Four");
		}

		[Test]
		public void TheOrderOfTheNumbersCantBeAffectResult ()
		{
			MakeSum ("Three", "One", "Two");
			MakeSum ("Three", "Two", "One");
		}

		[Test]
		public void IfSendOnePlusSixReturnsSeven ()
		{
			MakeSum ("Seven", "One", "Six");
		}

		[Test]
		public void IfSendFivePlusTenReturnsFifteen ()
		{
			MakeSum ("Fifteen", "Five", "Ten");
		}

		[Test]
		public void IfSendTenPlusOneReturnsEleven ()
		{
			MakeSum ("Eleven", "Ten", "One");
		}

		[Test]
		public void IfSendSixPlusSixReturnsTwelve ()
		{
			MakeSum ("Twelve", "Six", "Six");
		}

		[Test]
		public void IfSendTenPlusTenReturnsTwenty ()
		{
			MakeSum ("Twenty", "Ten", "Ten");
		}

		[Test]
		public void IfSendElevenPlusFourteenReturnsTwentyFive ()
		{
			MakeSum ("Twenty Five", "Eleven", "Fourteen");
		}

		[Test]
		public void IfSendTwelvePlusEighteenReturnsThirty ()
		{
			MakeSum ("Thirty", "Twelve", "Eighteen");
		}

		[Test]
		public void IfSendTwentyPlusTenReturnsThirty ()
		{
			MakeSum ("Thirty", "Twenty", "Ten");
		}

		[Test]
		public void IfSendTwentyTwoPlusTenReturnsThirtyThree ()
		{
			MakeSum ("Thirty Three", "Twenty Two", "Eleven");
		}

		[Test]
		public void IfSendFiftyFivePlusFortyTwoReturnsNinetySeven ()
		{
			MakeSum ("Ninety Seven", "Fifty Five", "Forty Two");
		}

		[Test]
		public void IfSendNinetyNinePlusOneReturnsOneHundred ()
		{
			MakeSum ("One Hundred", "Ninety Nine", "One");
		}

		[Test]
		public void IfSendNinetyNinePlusTwoReturnsOneHundredTwo ()
		{
			MakeSum ("One Hundred Two", "Ninety Nine", "Three");
		}

		[Test]
		public void IfSendNinetyPlusTwentyReturnsOneHundredTen ()
		{
			MakeSum ("One Hundred Ten", "Ninety", "Twenty");
		}

		[Test]
		public void IfSendNinetyOnePlusTwentyReturnsOneHundredEleven ()
		{
			MakeSum ("One Hundred Eleven", "Ninety One", "Twenty");
		}

		[Test]
		public void IfSendNinetyPlusFortyReturnsOneHundredThirty ()
		{
			MakeSum ("One Hundred Thirty", "Ninety", "Forty");
		}

		[Test]
		public void IfSendOneHundredPlusTwentyTwoReturnsOneHundredTwentyTwo ()
		{
			MakeSum ("One Hundred Twenty Two", "One Hundred", "Twenty Two");
		}


		[Test]
		public void IfSendFiftyPlusFiftyReturnsOneHundred ()
		{
			MakeSum ("One Hundred", "Fifty", "Fifty");
		}

		[Test]
		public void IfSendOneHundredPlusOneHundredReturnsTwoHundred ()
		{
			MakeSum ("Two Hundred", "One Hundred", "One Hundred");
		}

		private void MakeSum (string expected, string firstValue, string secondValue)
		{
			Assert.AreEqual (expected, converter.GetSum (firstValue, secondValue));
		}
	}
}
	