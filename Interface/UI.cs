﻿using System;

namespace NumberParser
{
	class UI
	{
		private static Converter converter = new Converter ();

		public static void Main (string[] args)
		{
			while (true)
			{
				Console.WriteLine ("Write First Number ");
				string firstDigit = Console.ReadLine ();
				Console.WriteLine ("\nWrite Second Number ");
				string secondDigit = Console.ReadLine ();
				Console.Beep (400, 800);
				Console.WriteLine ("\n\nResult: " + converter.GetSum (firstDigit, secondDigit));
				Console.Beep (800, 800);
				Console.WriteLine ("----------------------------------------------------------------\n\n\n");
			}
		}
	}
}
